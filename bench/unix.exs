unix = System.system_time(:second)
dt = DateTime.from_unix!(unix)
timezone = "America/New_York"

Benchee.run(%{
  fast_local_datetime: fn ->
    FastLocalDatetime.unix_to_datetime(unix, timezone)
  end,
  timex: fn ->
    unix
    |> DateTime.from_unix!()
    |> Timex.to_datetime(timezone)
  end,
  timex_only: fn ->
    Timex.to_datetime(dt, timezone)
  end,
  datetime: fn ->
    unix
    |> DateTime.from_unix!()
    |> DateTime.shift_zone(timezone, Tzdata.TimeZoneDatabase)
  end,
  datetime_only: fn ->
    DateTime.shift_zone(dt, timezone, Tzdata.TimeZoneDatabase)
  end
})
