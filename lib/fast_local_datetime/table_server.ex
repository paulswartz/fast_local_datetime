defmodule FastLocalDatetime.TableServer do
  @moduledoc """
  Maintains a table for a given timezone.
  """
  use GenServer
  alias FastLocalDatetime.{Table, TableRegistry}

  def start_link(timezone) do
    GenServer.start_link(__MODULE__, timezone)
  end

  def refresh(pid) do
    GenServer.call(pid, :refresh)
  end

  @impl GenServer
  def init(timezone) do
    case Table.new(timezone) do
      {:ok, table} ->
        Registry.register(TableRegistry, timezone, table)
        {:ok, timezone}

      {:error, error} ->
        {:stop, error}
    end
  end

  @impl GenServer
  def handle_call(:refresh, _from, timezone) do
    old_tables =
      for {_, table} <- Registry.lookup(TableRegistry, timezone) do
        table
      end

    {:ok, new_table} = Table.new(timezone)
    {_, _} = Registry.update_value(TableRegistry, timezone, fn _ -> new_table end)

    for table <- old_tables do
      Table.delete(table)
    end

    {:reply, :ok, timezone}
  end
end
