defmodule FastLocalDatetime.TableSupervisor do
  @moduledoc """
  DynamicSupervisor to manage the TableServer children.
  """
  use DynamicSupervisor

  def start_link([]) do
    DynamicSupervisor.start_link(__MODULE__, [], name: __MODULE__)
  end

  def start_child(timezone) do
    spec = %{
      id: :table_server,
      start: {FastLocalDatetime.TableServer, :start_link, [timezone]}
    }

    DynamicSupervisor.start_child(__MODULE__, spec)
  end

  def children do
    for {_, pid, _, _} <- DynamicSupervisor.which_children(__MODULE__), is_pid(pid) do
      pid
    end
  end

  @impl DynamicSupervisor
  def init([]) do
    DynamicSupervisor.init(strategy: :one_for_one)
  end
end
