# Changelog

## 1.0.1

- Ensure we support Elixir 1.10 and 1.11
- Address a deprecation warning with `simple_one_for_one` supervisor

## 1.0.0

- Increase minimum Elixir version to 1.8
- Update to Tzdata 1.0

## 0.3.0

- Fix bug with error returned before year 0 (fixed by @arkadyan)

## 0.2.0

- Fix a bug where timestamps before the year 0 would crash with a MatchError

## 0.1.0

- Initial release
