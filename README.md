# FastLocalDatetime

[![pipeline status](https://gitlab.com/paulswartz/fast_local_datetime/badges/main/pipeline.svg)](https://gitlab.com/paulswartz/fast_local_datetime/commits/main) [![coverage report](https://gitlab.com/paulswartz/fast_local_datetime/badges/main/coverage.svg)](https://gitlab.com/paulswartz/fast_local_datetime/commits/main)

Fast conversion of a UTC epoch timestamp (Unix timestamp) into a DateTime in
a given timezone.

## Examples

```elixir
iex> {:ok, dt} = unix_to_datetime(1522888537, "America/New_York")
iex> dt
#DateTime<2018-04-04 20:35:37-04:00 EDT America/New_York>

iex> unix_to_datetime(1522888537, "not found")
{:error, :not_found}
```

## Benchmarks

```
$ mix run bench/unix.exs

Operating System: macOS"
CPU Information: Intel(R) Core(TM) i7-7820HQ CPU @ 2.90GHz
Number of Available Cores: 8
Available memory: 16 GB
Elixir 1.9.0
Erlang 22.0

Benchmark suite executing with the following configuration:
warmup: 2 s
time: 5 s
memory time: 0 μs
parallel: 1
inputs: none specified
Estimated total run time: 35 s


Benchmarking datetime...
Benchmarking datetime_only...
Benchmarking fast_local_datetime...
Benchmarking timex...
Benchmarking timex_only...

Name                          ips        average  deviation         median         99th %
fast_local_datetime       69.93 K       14.30 μs    ±84.46%          13 μs          34 μs
datetime_only             31.07 K       32.19 μs    ±25.00%          31 μs          65 μs
datetime                  29.52 K       33.87 μs    ±23.98%          32 μs          68 μs
timex_only                10.28 K       97.26 μs    ±17.24%          95 μs         176 μs
timex                      9.85 K      101.53 μs    ±18.81%          98 μs         190 μs

Comparison:
fast_local_datetime       69.93 K
datetime_only             31.07 K - 2.25x slower
datetime                  29.52 K - 2.37x slower
timex_only                10.28 K - 6.80x slower
timex                      9.85 K - 7.10x slower
```

The `*_only` benchmarks don't include the conversion from a Unix timestamp to
a DateTime: they only test the timezone conversion of the given library.

## Installation

The package can be installed by adding `fast_local_datetime` to your list of
dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:fast_local_datetime, "~> 1.0.1"}
  ]
end
```

Documentation can be found at [https://hexdocs.pm/fast_local_datetime](https://hexdocs.pm/fast_local_datetime).

## Updates

`fast_local_datetime` keeps the tables up-to-date with `tzdata` by checking
periodically whether a new database has been loaded. By default, they're
checked every hour. This can be overriden in the configuration:

```elixir
# refresh once per day

config :fast_local_datetime, refresh_check_interval: 86_400_00
```
