defmodule FastLocalDatetime.RefresherTest do
  @moduledoc false
  use ExUnit.Case
  import FastLocalDatetime.Refresher
  alias FastLocalDatetime.TableRegistry

  @timezone "America/Anchorage"

  setup do
    env = Application.get_all_env(:fast_local_datetime)

    on_exit(fn ->
      for {key, value} <- env do
        Application.put_env(:fast_local_datetime, key, value)
      end
    end)
  end

  describe "init/1" do
    test "schedules a refresh" do
      Application.put_env(:fast_local_datetime, :refresh_check_interval, 10)
      assert {:ok, _} = init([])
      assert_receive :check_refresh
    end
  end

  describe "handle_info(:check_refresh)" do
    test "does a refresh when the versions don't match" do
      {:ok, _} = FastLocalDatetime.unix_to_datetime(0, @timezone)
      [{_, old_table}] = Registry.lookup(TableRegistry, @timezone)

      assert {:noreply, _} = handle_info(:check_refresh, "fake version")
      [{_, new_table}] = Registry.lookup(TableRegistry, @timezone)
      refute old_table == new_table
    end

    test "schedules a refresh" do
      {:ok, state} = init([])
      Application.put_env(:fast_local_datetime, :refresh_check_interval, 10)
      handle_info(:check_refresh, state)
      assert_receive :check_refresh
    end
  end

  describe "refresh/0" do
    test "refreshes each running TableServer" do
      {:ok, _} = FastLocalDatetime.unix_to_datetime(0, @timezone)
      [{_, old_table}] = Registry.lookup(TableRegistry, @timezone)
      refresh()
      [{_, new_table}] = Registry.lookup(TableRegistry, @timezone)
      refute old_table == new_table
    end
  end
end
