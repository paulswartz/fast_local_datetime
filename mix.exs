defmodule FastLocalDatetime.MixProject do
  use Mix.Project

  def project do
    [
      app: :fast_local_datetime,
      version: "1.0.1",
      elixir: "~> 1.8 or ~> 1.9 or ~> 1.10 or ~> 1.11",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      name: "FastLocalDatetime",
      description:
        "Fast conversion of a UTC epoch timestamp (Unix timestamp) into a DateTime in a given timezone.",
      source_url: "https://gitlab.com/paulswartz/fast_local_datetime",
      docs: [main: "readme", extras: ["README.md", "CHANGELOG.md"]],
      package: package(),
      test_coverage: [tool: ExCoveralls],
      preferred_cli_env: [coveralls: :test, "coveralls.html": :test]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {FastLocalDatetime.Application, []},
      env: [
        refresh_check_interval: 3_600_000
      ]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:tzdata, "~> 1.0"},
      {:ex_doc, "~> 0.19", optional: true, only: :dev, runtime: false},
      {:benchee, "~> 1.0.1", optional: true, only: :dev},
      {:timex, "~> 3.6", optional: true, only: :dev},
      {:excoveralls, "~> 0.11", optional: true, only: :test}
    ]
  end

  defp package do
    [
      licenses: ["Apache 2.0"],
      links: %{
        "GitLab" => "https://gitlab.com/paulswartz/fast_local_datetime"
      },
      maintainers: ["Paul Swartz <paul@paulswartz.net>"],
      files: ~w(lib/**/*.ex mix.exs README.md CHANGELOG.md LICENSE)
    ]
  end
end
